import Vue from 'vue'
import gsap from 'gsap'
import Router from 'vue-router'
import Blog from '@/views/blog'
import LandingPage from '../views/landingPage'
import About from '@/views/aboutUs'
import Contact from '@/views/contactUs'
import Projects from '@/views/projects'
import Realvest from '@/views/projects/components/projects/realvest'
import MenuBar from '@/components/MenuBar'
import BlogContentPage from '@/views/blog/components/innerPage'

Vue.use(Router)

const router =  new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'LandingPage',
      component: LandingPage,
      meta: {
        title: 'Home'
      }
    },
    {
      path: '/about',
      name: 'About',
      component: About,
      meta: {
        title: 'About'
      }
    },
    {
      path: '/contact',
      name: 'Cantact',
      component: Contact,
      meta: {
        title: 'Contact'
      }
    },
    {
      path: '/case-studies',
      name: 'Projects',
      component: Projects,
      meta: {
        title: 'Projects'
      }
    },
    {
      path: '/blog',
      name: 'Blog',
      component: Blog,
      meta: {
        title: 'Blog'
      }
    },
    {
      path: '/projects/:project_id',
      name: 'Realvest',
      component: Realvest
    },
    {
      path: '/blog/:blog_id/:blog_index',
      name: 'Blog-Content',
      component: BlogContentPage,
      meta: {
        title: 'Blog Post'
      }
    },
    {
      path: '/menu-bar',
      name: 'MenuBar',
      component: MenuBar
    }
  ]
})

export default router;
