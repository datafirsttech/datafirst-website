import gsap from 'gsap'
import ScrollTrigger from 'gsap/ScrollTrigger'
gsap.registerPlugin(ScrollTrigger)

export const myHelperFunction = {

  data(){
    return{
      nextPageTL: ''
    }
  },
  mounted () {

    this.pageScroll() 
  },

  methods: {
    pageScroll(){

    let pageWidth = window.innerWidth

    if(pageWidth < 1025) {
      window.scrollTo(0, 0)
      return false 
    }

    gsap.set("#footer", {clearProps: true})
    const sections = gsap.utils.toArray('.panel')
    const tl = gsap.timeline({
      scrollTrigger: {
        trigger: '.v-container',
        start: 'top top',
        pin: true,
        scrub: 1,
        paused:true,
        end: () => '+=' + document.querySelector('.v-container').offsetWidth
      }
    })
    tl.to(sections, {
      duration: 2,
      xPercent: -100 * (sections.length - 1.15),
      ease: 'none'
    })

    const tls = gsap.timeline({
      scrollTrigger: {
        trigger: '.banner-captions',
        scrub: true,
        start: '30% 100%',
        end: '300% 100%',
        paused:true,
      }
    })

    tls.to('.banner-img img', { height: '100vh', ease: 'none', duration: 1 }, '+=0.5')

    const timeline = gsap.timeline({
      scrollTrigger: {
        trigger: '.last-but-one',
        scrub: true,
        start: '630% 100%',
        end: '720% 100%',
      }
    })

    timeline
      .to('.grid-img', { opacity: 1, duration: 2, ease: 'power4' })
      .to('.grid-img', { opacity: 0, duration: 2, ease: 'power4' }, '+=0.5')

    tl.to('.last-panel', {
      yPercent: -60,
      ease: 'none',
    })

    // this.nextPageTL = gsap.timeline({
    //   scrollTrigger: {
    //     trigger: '#home-footer',
    //     scrub: true,
    //     start: '760% 100%',
    //     end: '795% 100%',
    //     paused:true,
    //     // markers: true,
    //     onLeave: self =>{
    //       this.$router.push({ path: 'about' })
    //       self.kill();
    //     }
    //   }
    // })

    // this.nextPageTL.to('#home-footer div', {opacity:0, duration:2})
    }
  },

  destroyed() {
    // this.nextPageTL.kill()
  }
}
