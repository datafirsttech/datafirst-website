import gsap from 'gsap'
import ScrollTrigger from 'gsap/ScrollTrigger'
gsap.registerPlugin(ScrollTrigger)

export const ContactPageFunction = {
  mounted () {
    // this.nextPage()
    
    let pageWidth = window.innerWidth;

    if(pageWidth < 1025) {
      window.scrollTo(0, 0)
      return false 
    }

    this.animatePage()
    const tl = gsap.timeline({
      scrollTrigger: {
        trigger: '.v-container',
        start: 'top top',
        pin: true,
        scrub: 1,
        end: () => '+=' + document.querySelector('.v-container').offsetWidth
      }
    })

    tl.to('.last-panel', {
      yPercent: -60,
      ease: 'none'
    })
  },

  methods : {
    // nextPage () {
    //   let nextPage = gsap.timeline({
    //     scrollTrigger: {
    //       trigger: '#footer',
    //       scrub: true,
    //       start: '200% 100%',
    //       end: '254% 80%',
    //       // markers: true,
    //       onLeave: self =>{
    //         this.$router.push('/blog')
    //         self.kill();
    //       }
    //     }
    //   })
  
    //   nextPage.to('.footer div', {opacity:0, duration:2})
    // },

    animatePage () {
      gsap.from('body', {overflow: 'hidden', duration: 1})
      gsap.from('.email', {xPercent: -30, delay:3, ease: 'power4', duration: 1.8, opacity:0 })
      gsap.from('.logo-brand', {width: '100vw', delay:0.5, ease: 'power4', duration: 1.7})
      gsap.from('.logo-brand h5', {xPercent: -90, delay:2, ease: 'power4', duration: 2, opacity:0 })

      gsap.from('.contact-caption h2', {yPercent:120, delay:1.5, ease: 'power4', duration: 1.5})
      gsap.from('.location', {xPercent: -30, delay:3.5, ease: 'power4', duration: 1.8, opacity:0 })
      gsap.from('.contact-caption p', {xPercent: -30, delay:2.7, ease: 'power4', duration: 2, opacity:0 })

      gsap.from('.working-hours', {display: 'none', delay:2})
      gsap.from('.working-hours small', {xPercent: -30, delay:4, ease: 'power4', duration: 2, opacity:0 })
      gsap.from('.working-hours li', {xPercent: -30, delay:3.5, ease: 'power4', duration: 1.5, opacity:0, stagger: 0.2 })
    }
  }
}
