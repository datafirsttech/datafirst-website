import gsap from 'gsap'
import ScrollTrigger from 'gsap/ScrollTrigger'
gsap.registerPlugin(ScrollTrigger)

export const myHelperFunction = {
  mounted () {
    let pageWidth = window.innerWidth;

    if(pageWidth < 1025) {
      window.scrollTo(0, 0)
      return false 
    }
    window.scrollTo(0, 0)

    this.animatePage()
    const sections = gsap.utils.toArray('.panel')
    const tl = gsap.timeline({
      scrollTrigger: {
        trigger: '.v-container',
        start: 'top top',
        pin: true,
        scrub: 1,
        end: () => '+=' + document.querySelector('.v-container').offsetWidth
      }
    })
    tl.to(sections, {
      duration: 2,
      xPercent: -100 * (sections.length - 1),
      ease: 'none'
    })

    const timeline = gsap.timeline({
      scrollTrigger: {
        trigger: '.last-but-one',
        scrub: true,
        start: '630% 100%',
        end: '720% 100%'
        // toggleActions: 'play reverse play reverse'
      }
    })

    timeline
      .to('.grid-img', { opacity: 1, duration: 2, ease: 'power4' })
      .to('.grid-img', { opacity: 0, duration: 2, ease: 'power4' }, '+=0.5')

    tl.to('.last-panel', {
      yPercent: -60,
      ease: 'none'
    })
  },

  methods: {
    animatePage(){
      gsap.from(".modal-container", {opacity: 0, ease:'Power2.out', duration: 1.2});
      // gsap.from(".swipper", {width:"0px", ease:'Power2.out', delay:0.9, duration: 1.2});
      // gsap.to(".swipper", {width:"0px", left:0, ease:'Power2.out', delay:0.9, duration: 1.2, delay: 2});
      // gsap.to(".swipper", {width:"100px", ease:'Power2.out', duration: 1.2});
    }
  }
}
