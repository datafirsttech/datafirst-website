import gsap from 'gsap'
import ScrollTrigger from 'gsap/ScrollTrigger'
gsap.registerPlugin(ScrollTrigger)

export const myHelperFunction = {
  data(){
    return{
      tl : '',
      nextPageTL:''
    }
  },
  mounted () {
    this.pageScroll()
    this.animatePage()
  },

  methods:{
    animatePage (){
     
      gsap.from('body', {overflow: 'hidden', duration: 1})
      gsap.from(".others", {xPercent: 40,  ease:'Power2.out', delay:3, duration: 0.7});
      gsap.from(".first-img", {width:"0px",  ease:'Pnone', delay:0.3, duration: 1});
      // gsap.from(".project-name", { yPercent:0,  ease:'Power2.out', opacity: 0, delay:0.9, duration: 1});
      
      gsap.from(".slide h1", {width:"0px", yPercent:100,  ease:'Power2.out', delay: 1.1, duration: 0.9});
      gsap.from(".slide2 h1", {width:"0px", yPercent:110,  ease:'Power2.out', delay:2.5, duration: 0.9});
      gsap.from(".first-project p", {xPercent:-100,  ease:'Power2.out', opacity: 0, delay:1.5, duration: 0.7, stagger:0.2});
    },

    pageScroll(){

      let pageWidth = window.innerWidth
  
      if(pageWidth < 1025) {
        window.scrollTo(0, 0)
        return false 
      }
  
      gsap.set("#footer", {clearProps: true})
      const sections = gsap.utils.toArray('.panel')
      this.tl = gsap.timeline({
        scrollTrigger: {
          trigger: '.v-container',
          start: 'top top',
          pin: true,
          scrub: 1,
          paused:true,
          end: () => '+=' + document.querySelector('.v-container').offsetWidth
        }
      })
      this.tl.to(sections, {
        duration: 2,
        xPercent: -100 * (sections.length -1),
        ease: 'none'
      })

      this.tl.to('.last-panel', {
        yPercent: -60,
        ease: 'none',
      })
  
      // this.nextPageTL = gsap.timeline({
      //   scrollTrigger: {
      //     trigger: '#home-footer',
      //     scrub: true,
      //     start: '760% 100%',
      //     end: '775% 70vh',
      //     paused:true,
      //     markers: true,
      //     onLeave: self =>{
      //       this.$router.push({ path: 'about' })
      //       self.kill();
      //     }
      //   }
      // })
  
      // this.nextPageTL.to('#home-footer div', {opacity:0, duration:2})
      },

    nextPage () {

      //  this.nextPageTL = gsap.timeline({
      //   scrollTrigger: {
      //     trigger: '#home-footer',
      //     scrub: true,
      //     start: '590% 100%',
      //     end: '733% 80%',
      //     // markers: true,
      //     onLeave: self =>{
      //       this.$router.push({ path: 'contact' })
      //       self.kill();
      //     }
      //   }
      // })
  
      // this.nextPageTL.to('#home-footer div', {opacity:0, duration:2})
    }
  },

  destroyed() {
    let pageWidth = window.innerWidth
  
      if(pageWidth < 1025) {
        return false 
      }
    this.tl.kill()
    // this.nextPageTL.kill()
  }
  
}
