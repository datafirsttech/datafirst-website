import gsap from 'gsap'
import ScrollTrigger from 'gsap/ScrollTrigger'
gsap.registerPlugin(ScrollTrigger)

export const AboutPageFunction = {
  data(){
    return{
      nextPageTL : ''
    }
  },
  mounted () {

    let pageWidth = window.innerWidth;
    let currentpage = this.$route.path

    if(pageWidth < 1025) {
      window.scrollTo(0, 0)
      return false 
    }

    if(currentpage != '/about'){
      return false
    }

    this.animatePage()
    const sections = gsap.utils.toArray('.panel')
    const tl = gsap.timeline({
      scrollTrigger: {
        trigger: '.v-container',
        start: 'top top',
        pin: true,
        scrub: 1,
        paused:true,
        end: () => '+=' + document.querySelector('.v-container').offsetWidth
      }
    })
    
    tl.to(sections, {
      duration: 2,
      xPercent: -100 * (sections.length - 1),
      ease: 'none'
    })

    const timeline = gsap.timeline({
      scrollTrigger: {
        trigger: '.last-but-one',
        scrub: true,
        start: '630% 100%',
        end: '720% 100%',
        paused:true
        // toggleActions: 'play reverse play reverse'
      }
    })

    timeline
      .to('.grid-img', { opacity: 1, duration: 2, ease: 'power4' })
      .to('.grid-img', { opacity: 0, duration: 2, ease: 'power4' }, '+=0.5')

    tl.to('.last-panel', {
      yPercent: -60,
      ease: 'none'
    })

    // this.nextPageTL = gsap.timeline({
    //   scrollTrigger: {
    //     trigger: '#footer',
    //     scrub: true,
    //     start: '427% 100%',
    //     end: '428% 80%',
    //     paused:true,
    //     onLeave: self => {
    //       this.$router.push({ path: 'projects' })
    //       self.kill();
    //     }
          

    //   }
    // })

    // this.nextPageTL.to('#footer div', {opacity:0, duration:2})
    
  },

  methods: {
    animatePage() {
      gsap.from('body', {overflow: 'hidden', duration: 1})
      gsap.from('.descriptions span', {opacity:0, duration:2, ease: 'power4', delay:5, yPercent: -30})
      gsap.from('.logo-brand ', {width: '100vw', duration: 2, ease: 'power4', delay:0.5})
      gsap.from('.slide h2 ', {yPercent: 100, duration:1, delay: 1.5 })
      
      gsap.from('.slide2 h5 ', {xPercent: -40, duration:1, delay: 2, opacity:0 })
      gsap.from('.banner-img img', {xPercent: '30', ease: 'back.out(1.2)', delay:3, duration:2 })
      gsap.from('.banner-captions p', {opacity: 0, duration:1.5, ease: 'power2.out', delay:2.5, yPercent: 20})
    }
  },

  destroyed() {
    // this.nextPageTL.kill()
  }
}
